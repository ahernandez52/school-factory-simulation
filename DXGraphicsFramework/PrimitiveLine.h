#pragma once
#include "d3dUtil.h"

class PrimitiveLine
{
private:

	VertexPNTC m_StartingPos;
	VertexPNTC m_EndingPos;

	D3DCOLOR m_Color;

public:
	PrimitiveLine(D3DXVECTOR3 start, D3DXVECTOR3 end, D3DCOLOR color);
	~PrimitiveLine();

	D3DXVECTOR3 StartPosition() const;	
	VertexPNTC	GetStartVertex();
	void		SetStartPosition(D3DXVECTOR3 newPos);	
	void		SetStartPosition(float x, float y, float z);

	D3DXVECTOR3 EndPosition() const;
	VertexPNTC	GetEndVertex();
	void		SetEndPosition(D3DXVECTOR3 newPos);
	void		SetEndPosition(float x, float y, float z);

	D3DCOLOR	Color() const;
	void		SetColor(D3DCOLOR newColor);
	void		SetColor(int r, int g, int b);
};
#pragma once
#include "d3dApp.h"
#include <tchar.h>
#include <crtdbg.h>
#include "GameStateMachine.h"
#include "DirectInput.h"
#include "Camera.h"
#include "XBOXController.h"
#include "RenderPipline.h"


class DXMain : public D3DApp
{
private:

	GameStateMachine	*GSM;
	RenderPipline		*m_RenderPipline;

	bool				m_paused;

private:

	void EventXBOXController();

public:

	DXMain(HINSTANCE hInstance, std::string winCaption, D3DDEVTYPE devType, DWORD requestedVP);
	~DXMain();

	bool				checkDeviceCaps();
	void				onLostDevice();
	void				onResetDevice();
	void				updateScene(float dt);
	void				drawScene();

	void				startGSM();
	GameStateMachine	*getGSM();
};
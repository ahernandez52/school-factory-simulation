#pragma once
#include "d3dUtil.h"
#include "PrimitiveMesh.h"
#include "PrimitiveLine.h"
#include "GfxStats.h"
#include "D3D9ShaderEffect.h"
#include <vector>
#include <map>
#include <string>


class RenderPipline
{
friend class GraphicsFramework;
private:

	std::map<std::string, MeshSchematic>				m_Schematics;
	std::map<std::string, std::vector<PrimitiveMesh*>>	m_GraphicsObjects;
	std::vector<std::vector<PrimitiveMesh*>*>			m_AllGraphicsObjects;
	std::vector<PrimitiveLine*>							m_LineObjects;

	LPDIRECT3DTEXTURE9	m_defaultTexture;

	ID3DXLine			*m_LineDrawer;

	DirectionalLight	m_light;
	MeshMaterial		m_material;
	GfxStats			*m_GfxStats;

	D3D9ShaderEffect	*m_FX;

private:

	//Internal Functions
	void BuildFX();
	void BuildDefaultSchematics();

	//Schematic Functions
	void RegisterSchematic(MeshSchematic Schematic);
	MeshSchematic GetSchematic(std::string SchematicIdentifier);

	//Registration Functions
	void RegisterLine(PrimitiveLine *newLine);
	void RegisterMesh(PrimitiveMesh *newMesh);

	//Deletion Functions
	void RemoveLine(PrimitiveLine *Line);
	void RemoveMesh(PrimitiveMesh *Mesh);

	//Getter Functions
	PrimitiveMesh* GetMeshByID(int id);
	PrimitiveLine* GetLineByID(int id);

	//Render Helper Functions
	void RenderLines();

public:
	RenderPipline();
	~RenderPipline();

	void OnResetDevice();
	void OnLostDevice();

	void Update(float dt);
	void Render();
};


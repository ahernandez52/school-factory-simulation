#pragma once
#include "d3dUtil.h"
#include "GraphicsFramework.h"

class PrimitiveMesh
{
private:
	D3DXVECTOR3 m_Position;
	D3DXVECTOR3 m_Scale;
	D3DXVECTOR3 m_Heading;

	D3DXMATRIX m_TransformMatrix;

	MeshMaterial m_Material;

	D3DXQUATERNION m_Orientaiton;

	LPDIRECT3DTEXTURE9 m_DiffuseTexture;

	D3DXCOLOR m_Color;

	ID3DXMesh *m_Mesh;

	float m_Radius;

	int m_ID;

	bool m_Wireframe;

	std::string m_SchematicID;
	
public:

	PrimitiveMesh(std::string Type, D3DXVECTOR3 pos, D3DXVECTOR3 head, D3DXVECTOR3 scale)
	{
		m_SchematicID = Type;

		static int nextID = 0;
		m_ID = nextID;
		nextID++;

		MeshSchematic Schematic = GFI->GetSchematic(Type);

		m_Mesh					= Schematic.mesh;
		m_DiffuseTexture		= Schematic.texture;

		m_Position				= pos;
		m_Scale					= scale;

		m_Color					= D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f);

		m_Material.ambient		= D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f);
		m_Material.diffuse		= D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f);
		m_Material.spec			= D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
		m_Material.specPower	= 2.0f;

		D3DXQuaternionIdentity(&m_Orientaiton);

		m_Wireframe = false;

		GFI->RegisterMesh(this);
	}

	~PrimitiveMesh() {}

	void buildTransformMatrix()
	{
		D3DXMATRIX R, T, S;

		D3DXQuaternionNormalize(&m_Orientaiton, &m_Orientaiton);
		D3DXMatrixRotationQuaternion(&R, &m_Orientaiton);

		D3DXMatrixTranslation(&T, m_Position.x, m_Position.y, m_Position.z);

		D3DXMatrixScaling(&S, m_Scale.x, m_Scale.y, m_Scale.z);

		m_TransformMatrix = S * R * T;
	}

	D3DXMATRIX		GetTransform()			{ return m_TransformMatrix; }
	MeshMaterial	GetMaterial()			{ return m_Material; }
	ID3DXMesh*		GetMesh()				{ return m_Mesh; }
	LPDIRECT3DTEXTURE9* GetTexture()			{ return &m_DiffuseTexture; }
	std::string		GetSchemIdentifier()	{ return m_SchematicID; }
	bool			WireframeMode()			{ return m_Wireframe; }

	void			SetWireframe(bool mode)	{ m_Wireframe = mode; }

	//Getters & Setters
	//==============================
	D3DXVECTOR3		Position() const						{ return m_Position; }
	void			SetPosition(D3DXVECTOR3 newPos)			{ m_Position = newPos; }
	void			SetPosition(float x, float y, float z)	{ m_Position = D3DXVECTOR3(x, y, z); }

	float			Radius() const							{ return m_Radius; }
	D3DXVECTOR3		Scale() const							{ return m_Scale; }
	void			SetRadius(float value)					{ m_Radius = value; }
	void			SetScale(D3DXVECTOR3 scale)				{ m_Scale = scale; }
	void			SetScale(float x, float y, float z)		{ m_Scale = D3DXVECTOR3(x, y, z); }

	D3DXVECTOR3		Heading() const							{ return m_Heading; }
	void			SetHeading(D3DXVECTOR3 value)			{ D3DXVec3Normalize(&m_Heading, &value); }
	void			SetHeading(D3DXQUATERNION quat)			{ m_Orientaiton = quat; }
	void			SetHeading(float value)
	{
		D3DXMATRIX R;
		D3DXVECTOR3 A(1.0f, 0.0f, 0.0f);

		D3DXMatrixRotationZ(&R, value);

		D3DXVec3TransformCoord(&m_Heading, &A, &R);
	}

	void			SetHeading(float yaw, float pitch, float roll)
	{
		D3DXQuaternionRotationYawPitchRoll(&m_Orientaiton, yaw, pitch, roll);
	}

	D3DXCOLOR		Color() const								{ return m_Color; }
	void			SetColor(D3DXCOLOR color)					{ m_Color = color; }
	void			SetColor(FLOAT r, FLOAT g, FLOAT b, FLOAT a){ m_Color = D3DXCOLOR(r, g, b, a); }
	//******************************
};
#include "PrimitiveLine.h"
#include "GraphicsFramework.h"

PrimitiveLine::PrimitiveLine(D3DXVECTOR3 start, D3DXVECTOR3 end, D3DCOLOR color)
{
	m_Color			= color;
	m_StartingPos	= VertexPNTC(start, D3DXVECTOR3(0.0f, 1.0f, 0.0f), D3DXVECTOR2(0.0f, 1.0f), m_Color);
	m_EndingPos		= VertexPNTC(end, D3DXVECTOR3(0.0f, 1.0f, 0.0f), D3DXVECTOR2(0.0f, 1.0f), m_Color);
		
	GFI->RegisterLine(this);
}

PrimitiveLine::~PrimitiveLine()
{
	GFI->RemoveLine(this);
}

//Accessor Functions
//==================================
D3DXVECTOR3 PrimitiveLine::StartPosition() const						{ return m_StartingPos.pos; }
VertexPNTC	PrimitiveLine::GetStartVertex()								{ return m_StartingPos; }
void		PrimitiveLine::SetStartPosition(D3DXVECTOR3 newPos)			{ m_StartingPos.pos = newPos; }
void		PrimitiveLine::SetStartPosition(float x, float y, float z)	{ m_StartingPos.pos = D3DXVECTOR3(x, y, z); }

D3DXVECTOR3 PrimitiveLine::EndPosition() const							{ return m_EndingPos.pos; }
VertexPNTC	PrimitiveLine::GetEndVertex()								{ return m_EndingPos; }
void		PrimitiveLine::SetEndPosition(D3DXVECTOR3 newPos)			{ m_EndingPos.pos = newPos; }
void		PrimitiveLine::SetEndPosition(float x, float y, float z)	{ m_EndingPos.pos = D3DXVECTOR3(x, y, z); }

D3DCOLOR	PrimitiveLine::Color() const								{ return m_Color; }
void		PrimitiveLine::SetColor(D3DCOLOR newColor)					{ m_Color = newColor; }
void		PrimitiveLine::SetColor(int r, int g, int b)				{ m_Color = D3DCOLOR_XRGB(r, g, b); }
//**********************************
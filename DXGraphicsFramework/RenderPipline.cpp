#include "RenderPipline.h"
#include "Camera.h"

using namespace std;

RenderPipline::RenderPipline()
{
	//Build default Schematics
	BuildDefaultSchematics();

	//Load the Shader
	BuildFX();

	//Setup default light
	m_light.ambient = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
	m_light.diffues = D3DXCOLOR(0.8f, 0.8f, 0.8f, 1.0f);
	m_light.specular = D3DXCOLOR(0.8f, 0.8f, 0.8f, 1.0f);
	m_light.directionWorld = D3DXVECTOR3(1.0f, 0.0f, 0.0f);

	m_FX->SetParamaterValue("gLight", &m_light, sizeof(DirectionalLight));

	
	m_GfxStats = new GfxStats();

	InitAllVertexDeclarations();

	HR(D3DXCreateLine(gd3dDevice, &m_LineDrawer));
}


RenderPipline::~RenderPipline()
{
	ReleaseCOM(m_defaultTexture);

	for(auto Schematic : m_Schematics)
		ReleaseCOM(Schematic.second.mesh);

	delete m_GfxStats;
}

// Internal Functions
//================================
void RenderPipline::BuildFX()
{
	m_FX = new D3D9ShaderEffect("PhongShader.fx", gd3dDevice);

	//technique
	m_FX->CreateNewTechnique("PhongDirLtTexTechFilled");
	m_FX->CreateNewTechnique("PhongDirLtTexTechWireframe");

	//transform and view matracies
	m_FX->CreateNewParamater("gWVP");
	m_FX->CreateNewParamater("gWorld");
	m_FX->CreateNewParamater("gWorldInv");

	//camera eye
	m_FX->CreateNewParamater("gEyePos");

	//material handles
	m_FX->CreateNewParamater("gMtrl");
	m_FX->CreateNewParamater("gLight");

	//texture handles
	m_FX->CreateNewParamater("gTex");
}

void RenderPipline::BuildDefaultSchematics()
{
	MeshSchematic tempSchematic;
	vector<PrimitiveMesh*> BoxArray, SphereArray;

	D3DXCreateTextureFromFile(gd3dDevice, "whitetex.dds", &m_defaultTexture);

	//assign default white texture to all mesh schematics
	tempSchematic.texture = m_defaultTexture;

		// === Box Declarations ==========================
	//Create Box Mesh and assign it to the Box Schematic
	D3DXCreateBox(gd3dDevice, 1.0f, 1.0f, 1.0f, &tempSchematic.mesh, NULL);	

	//Insert Box Schematic into Schematic Map
	m_Schematics.insert(make_pair("Box", tempSchematic));

	//Insert vector of Primitive meshes that will use Box Schematic
	m_GraphicsObjects.insert(make_pair("Box", BoxArray));

	//Add schematic specific vector to All Objects vector
	m_AllGraphicsObjects.push_back(&m_GraphicsObjects.at("Box"));
	//************************************************

	// === Sphere Declarations =======================
	//Create SPhere Mesh and assign it to the Sphere Schematics
	D3DXCreateSphere(gd3dDevice, 1.0f, 12, 12, &tempSchematic.mesh, NULL);

	//Insert Sphere Schematic into Schematic Map
	m_Schematics.insert(make_pair("Sphere", tempSchematic));

	//Insert vector of Primitve meshes that will use Sphere Schematic
	m_GraphicsObjects.insert(make_pair("Sphere", SphereArray));

	//Add schematic specific vector to All Objects vector
	m_AllGraphicsObjects.push_back(&m_GraphicsObjects.at("Sphere"));
	//************************************************
}

//Schematic Functions
void RenderPipline::RegisterSchematic(MeshSchematic Schematic)
{

}

MeshSchematic RenderPipline::GetSchematic(std::string SchematicIdentifier)
{
	return m_Schematics.at(SchematicIdentifier);
}

//Registration Functions
void RenderPipline::RegisterLine(PrimitiveLine *newLine)
{
	m_LineObjects.push_back(newLine);
}

void RenderPipline::RegisterMesh(PrimitiveMesh *newMesh)
{
	m_GraphicsObjects.at(newMesh->GetSchemIdentifier()).push_back(newMesh);
}

//Deletion Functions
void RenderPipline::RemoveLine(PrimitiveLine *Line)
{
	
}

void RenderPipline::RemoveMesh(PrimitiveMesh *Mesh)
{

}

//Getter Functions
PrimitiveMesh* RenderPipline::GetMeshByID(int id)
{
	return nullptr;
}

PrimitiveLine* RenderPipline::GetLineByID(int id)
{
	return nullptr;
}

//Render Helper Functions
void RenderPipline::RenderLines()
{
	D3DXMATRIX T = gCamera->viewProj();

	for(auto Line : m_LineObjects)
	{
		D3DXVECTOR3 points[2];
		points[0] = Line->StartPosition();
		points[1] = Line->EndPosition();

		m_LineDrawer->DrawTransform(points, 2, &T, Line->Color());
	}
}
//********************************

// Public Functions
//================================
void RenderPipline::OnResetDevice()
{
	m_FX->OnResetDevice();
	m_GfxStats->onResetDevice();
	m_LineDrawer->OnResetDevice();
}

void RenderPipline::OnLostDevice()
{
	m_FX->OnLostDevice();
	m_GfxStats->onLostDevice();
	m_LineDrawer->OnLostDevice();
}

void RenderPipline::Update(float dt)
{
	m_GfxStats->update(dt);
}

void RenderPipline::Render()
{
	D3DXMATRIX W, WI;

	HR(gd3dDevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DXCOLOR(0.0f, 0.0f, .39f, 1.0f), 1.0f, 0));

	HR(gd3dDevice->BeginScene());

	m_GfxStats->display();

	m_FX->SetParamaterValue("gEyePos", gCamera->pos(), sizeof(D3DXVECTOR3));

	int MatrixSize = sizeof(D3DXMATRIX);
	int MaterialSize = sizeof(MeshMaterial);
	int TextureSize = sizeof(LPDIRECT3DTEXTURE9);

	D3DXMATRIX viewProj = gCamera->viewProj();

	for(auto vectorOfType : m_AllGraphicsObjects)
	{
		for(auto Object : *vectorOfType)
		{
			if(Object->WireframeMode())
				m_FX->SetTechnique("PhongDirLtTexTechWireframe");
			else
				m_FX->SetTechnique("PhongDirLtTexTechFilled");
			
			m_FX->Begin(0);
			m_FX->BeginPass(0);

			Object->buildTransformMatrix();

			D3DXMatrixIdentity(&W);

			W = W * Object->GetTransform();

			m_FX->SetParamaterValue("gWorld", &W, MatrixSize);
			m_FX->SetParamaterValue("gWVP", &(W * viewProj), MatrixSize);

			D3DXMatrixInverse(&WI, 0, &W);
			m_FX->SetParamaterValue("gWorldInv", &WI, MatrixSize);

			m_FX->SetParamaterValue("gTex", Object->GetTexture(), TextureSize);
			m_FX->SetParamaterValue("gMtrl", &Object->GetMaterial(), MaterialSize);

			m_FX->CommitChanges();

			Object->GetMesh()->DrawSubset(0);

			m_FX->EndPass();
			m_FX->End();
		}
	}

	RenderLines();

	HR(gd3dDevice->EndScene());
	HR(gd3dDevice->Present(0, 0, 0, 0));
}
//********************************
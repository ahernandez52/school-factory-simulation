#pragma once

class DXMain;
class PrimitiveLine;
class PrimitiveMesh;

class GameState{

public:
	GameState(){}
	~GameState(){}

	virtual void InitializeState(DXMain *owner) = 0;
    virtual void UpdateScene(float dt) = 0;
    virtual void LeaveState() = 0;
};


#pragma once
#include "GameState.h"


class GameStateMachine{
private:

	GameState	*m_CurrentState;
	GameState	*m_PreviousState;
	DXMain		*m_OwningApp;

public:
	GameStateMachine(DXMain *OwningApp):m_CurrentState(0),
										m_PreviousState(0),
										m_OwningApp(OwningApp){}

	virtual ~GameStateMachine()
	{
		delete m_PreviousState;
		delete m_CurrentState;
	}

	void SetCurrentState(GameState* gs)
	{
		m_CurrentState = gs;
	}

	void SetPreviousState(GameState* gs)
	{
		m_PreviousState = gs;
	}

	void InitializeState()const
	{
		m_CurrentState->InitializeState(m_OwningApp);
	}

	void UpdateScene(float dt)const
	{
		m_CurrentState->UpdateScene(dt);
	}

	void LeaveState()const
	{
		m_CurrentState->LeaveState();
	}

	void ChangeState(GameState* newState)
	{
		delete m_PreviousState;

		m_PreviousState = m_CurrentState;
		m_CurrentState->LeaveState();
		m_CurrentState = newState;
		m_CurrentState->InitializeState(m_OwningApp);
	}

	void ReverToPreviousState()
	{
		//m_PreviousState = m_CurrentState;
		m_CurrentState->LeaveState();

		GameState* temp = m_CurrentState;

		m_CurrentState = m_PreviousState;
		m_PreviousState = temp;

		m_CurrentState->InitializeState(m_OwningApp);
	}
};


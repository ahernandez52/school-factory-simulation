#pragma once
#include "d3dUtil.h"

#define GFI GraphicsFramework::Instance()

class RenderPipline;
class PrimitiveMesh;
class PrimitiveLine;

class GraphicsFramework
{
private:

	RenderPipline *m_RenderPipeline;

private:

	GraphicsFramework(){}

public:

	static GraphicsFramework* Instance();
	~GraphicsFramework(){}

	void Init(RenderPipline* pipeline);

	//Schematic Functions
	void RegisterSchematic(MeshSchematic Schematic);
	MeshSchematic GetSchematic(std::string SchematicIdentifier);

	//Registration Functions
	void RegisterLine(PrimitiveLine *newLine);
	void RegisterMesh(PrimitiveMesh *newMesh);

	//Deletion Functions
	void RemoveLine(PrimitiveLine *Line);
	void RemoveMesh(PrimitiveMesh *Mesh);

	//Getter Functions
	PrimitiveMesh* GetMeshByID(int id);
	PrimitiveLine* GetLineByID(int id);
};


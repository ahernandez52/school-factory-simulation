//=============================================================================
// d3dUtil.h by Frank Luna (C) 2005 All Rights Reserved.
//
// Contains various utility code for DirectX applications, such as, clean up
// and debugging code.
//=============================================================================

#ifndef D3DUTIL_H
#define D3DUTIL_H

// Enable extra D3D debugging in debug builds if using the debug DirectX runtime.  
// This makes D3D objects work well in the debugger watch window, but slows down 
// performance slightly.
#if defined(DEBUG) | defined(_DEBUG)
#ifndef D3D_DEBUG_INFO
#define D3D_DEBUG_INFO
#endif
#endif

#include <io.h>
#include <fcntl.h>
#include <stdio.h>
#include <iostream>

#include <d3d9.h>
#include <d3dx9.h>
#include <dxerr9.h>
#include <sstream>

#include "Vertex.h"

//===============================================================
// Globals for convenient access.
class D3DApp;
extern D3DApp* gd3dApp;
extern IDirect3DDevice9* gd3dDevice;

//===============================================================
// Clean up

#define ReleaseCOM(x) { if(x){ x->Release();x = 0; } }

// ========== Materials and Lights ==========
struct DirectionalLight
{
	D3DXCOLOR ambient;
	D3DXCOLOR diffues;
	D3DXCOLOR specular;
	D3DXVECTOR3 directionWorld;
};

struct MeshMaterial
{
	D3DXCOLOR ambient;	//Ka
	D3DXCOLOR diffuse;	//Kd
	D3DXCOLOR spec;		//Ks
	float specPower;	//Ns
};

struct PointLight
{
	D3DXCOLOR ambient;
	D3DXCOLOR diffuse;
	D3DXCOLOR spec;
	D3DXVECTOR3 posW;
	float range;
};
//===Engine Schematics===========================================
struct MeshSchematic
{
	std::string			Identifier;
	ID3DXMesh			*mesh;
	LPDIRECT3DTEXTURE9	texture;
};
//===============================================================

//===Math Helper Functions=======================================
D3DXVECTOR3 QuatForwardVec(D3DXQUATERNION q);
D3DXVECTOR3 QuatUpVec(D3DXQUATERNION q);
D3DXVECTOR3 QuatRightVec(D3DXQUATERNION q);
//===============================================================

// Debug

#if defined(DEBUG) | defined(_DEBUG)
	#ifndef HR
	#define HR(x)                                      \
	{                                                  \
		HRESULT hr = x;                                \
		if(FAILED(hr))                                 \
		{                                              \
			DXTrace(__FILE__, __LINE__, hr, #x, TRUE); \
		}                                              \
	}
	#endif

#else
	#ifndef HR
	#define HR(x) x;
	#endif
#endif 

#endif // D3DUTIL_H
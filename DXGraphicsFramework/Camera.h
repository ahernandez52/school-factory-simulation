#pragma once
#include "d3dUtil.h"

#define gCamera Camera::Instance()

enum CameraModes{
	FIXED_ROTATION,
	FREEFORM,
	ATTACHED,
	STANDSTILL
};

enum CameraInput{
	XBOX_CONTROLLER,
	PC_MOUSEKEYBOARD,
	BOTH,
};

class Camera{
public:
	static Camera* Instance();
	~Camera();

	D3DXMATRIX view();
	D3DXMATRIX proj();
	D3DXMATRIX viewProj();

	D3DXVECTOR3 right();
	D3DXVECTOR3 up();
	D3DXVECTOR3 look();

	D3DXVECTOR3& pos();
	float& fixedDist();

	void setMode(CameraModes newMode);
	void setInput(CameraInput newInput);
	void setSpeed(float s);
	void setFOV(float fov);
	void setFrustum(float nearZ, float farZ);
	void setLens(float fieldOfView, float aspectRatio, float closeZ, float farZ);
	void toggleAxis();
	void lookAt(D3DXVECTOR3& pos, D3DXVECTOR3& target, D3DXVECTOR3& up);
	void SnapToLookAtPoint(D3DXVECTOR3 snapPoint);

	void update(float dt);

	void onResetDevice(float w, float h);

protected:
	void buildView();
	void buildThirdPerson();
	void buildWorldFrustumPlanes();

	void adjustCamera(float dt);
	void rotateCamera(float dt);

	void adjustFixedCamera(float dt);
	void rotateFixedCamera(float dt);

protected:
	D3DXMATRIX m_view;
	D3DXMATRIX m_proj;
	D3DXMATRIX m_viewProj;

	//world space coords
	D3DXVECTOR3 m_posW;
	D3DXVECTOR3 m_rightW;
	D3DXVECTOR3 m_upW;
	D3DXVECTOR3 m_lookW;

	float m_speed;
	float m_fov;
	float m_nearZ;
	float m_farZ;

	float m_fixedPointTheta;
	float m_fixedPointPhi;
	float m_fixedPointDistance;

	D3DXVECTOR3 m_fixedLookPoint;

	bool m_axisAlligned;

	CameraModes m_cameraMode;
	CameraInput m_cameraInput;

	D3DXPLANE m_frustumPlanes[6];

private:
	Camera();
};


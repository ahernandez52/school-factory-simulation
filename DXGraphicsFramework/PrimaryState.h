#pragma once
#include "gamestate.h"

class PrimaryState : public GameState
{
private:

	DXMain*			m_owningApp;

	PrimitiveMesh *theBox;
	PrimitiveLine *theLine;

private:

public:

public:

	virtual void InitializeState(DXMain *owner);
    virtual void UpdateScene(float dt);
    virtual void LeaveState();
};


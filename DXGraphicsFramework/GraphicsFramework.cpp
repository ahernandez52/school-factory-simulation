#include "GraphicsFramework.h"
#include "RenderPipline.h"
#include "PrimitiveLine.h"
#include "PrimitiveMesh.h"

GraphicsFramework* GraphicsFramework::Instance()
{
	static GraphicsFramework temp;
	return &temp;
}

void GraphicsFramework::Init(RenderPipline* pipeline)
{
	//call init functions
	m_RenderPipeline = pipeline;
}

// Schematic Functions
//==================================
void GraphicsFramework::RegisterSchematic(MeshSchematic Schematic)
{
	m_RenderPipeline->RegisterSchematic(Schematic);
}

MeshSchematic GraphicsFramework::GetSchematic(std::string SchematicIdentifier)
{
	return m_RenderPipeline->GetSchematic(SchematicIdentifier);
}
//**********************************

// Registration Functions
//==================================
void GraphicsFramework::RegisterLine(PrimitiveLine *newLine)
{
	m_RenderPipeline->RegisterLine(newLine);
}

void GraphicsFramework::RegisterMesh(PrimitiveMesh*newMesh)
{
	m_RenderPipeline->RegisterMesh(newMesh);
}
//**********************************

// Deletion Functions
//==================================
void GraphicsFramework::RemoveLine(PrimitiveLine *Line)
{
	m_RenderPipeline->RemoveLine(Line);
}

void GraphicsFramework::RemoveMesh(PrimitiveMesh *Mesh)
{
	m_RenderPipeline->RemoveMesh(Mesh);
}
//**********************************

// Getter Functions
//==================================
PrimitiveMesh* GraphicsFramework::GetMeshByID(int id)
{
	return m_RenderPipeline->GetMeshByID(id);
}

PrimitiveLine* GraphicsFramework::GetLineByID(int id)
{
	return m_RenderPipeline->GetLineByID(id);
}
//**********************************
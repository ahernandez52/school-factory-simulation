#include "PrimaryState.h"
#include "DXMain.h"
#include "Camera.h"


void PrimaryState::InitializeState(DXMain *owner)
{
	m_owningApp = owner;

	theBox = new PrimitiveMesh("Box", D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(1.0f, 0.0f, 0.0f), D3DXVECTOR3(10.0f, 10.0f, 10.0f));
}

void PrimaryState::UpdateScene(float dt)
{

}

void PrimaryState::LeaveState()
{

}
